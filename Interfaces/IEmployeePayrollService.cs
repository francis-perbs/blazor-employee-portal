﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Interfaces
{
    interface IEmployeePayrollService
    {
        public Task<IEnumerable<EmployeePayroll>> GetEmployeePayrollsAsync();
    }
}
