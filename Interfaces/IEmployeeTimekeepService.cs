﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Interfaces
{
    interface IEmployeeTimekeepService
    {
        public Task<IEnumerable<EmployeeTimekeep>> GetEmployeeTimekeepsAsync();
    }
}
