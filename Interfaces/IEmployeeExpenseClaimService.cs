﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Interfaces
{
    interface IEmployeeExpenseClaimService
    {
        public Task<IEnumerable<EmployeeExpenseClaim>> GetEmployeeExpenseClaimsAsync();
    }
}
