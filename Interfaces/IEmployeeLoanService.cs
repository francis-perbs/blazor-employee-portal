﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Interfaces
{
    interface IEmployeeLoanService
    {
        public Task<IEnumerable<EmployeeLoan>> GetEmployeeLoansByEmpIDAsync(int empID);
    }
}
