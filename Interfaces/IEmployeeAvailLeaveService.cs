﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Interfaces
{
    interface IEmployeeAvailLeaveService
    {
        public Task<IEnumerable<EmployeeAvailLeave>> GetEmployeeLeavesByEmpIDAsync(int empID);
    }
}
