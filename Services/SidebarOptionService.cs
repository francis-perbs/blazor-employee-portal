﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Services
{
    public class SidebarOptionService
    {
        SidebarOption[] allOptions = new[] {
            new SidebarOption() {
                Name = "Dashboard",
                Path = "/",
                Icon = "&#xe88a",
                Enabled = true
            },
            new SidebarOption() {
                Name = "Profiles",
                Icon = "&#xe55a",
                Children = new [] {
                    new SidebarOption() {
                        Name = "Employees",
                        Path = "/profiles/employees",
                        Icon = "&#xe87c",
                        Enabled = true
                    }//,
                    //new SidebarOption() {
                    //    Name = "Customers",
                    //    Path = "/profiles/customers",
                    //    Icon = "&#xe8a6",
                    //    Enabled = false
                    //}
                }
            },
            new SidebarOption() {
                Name = "Timekeeping",
                Path = "/timekeeping",
                Icon = "&#xe8df",
                Enabled = true
            },
            new SidebarOption() {
                Name = "Payroll",
                Path = "/payroll",
                Icon = "&#xe85d",
                Enabled = true
            },
            new SidebarOption() {
                Name = "Expense Claims",
                Path = "/expense-claims",
                Icon = "&#xe873",
                Enabled = true
            },
            new SidebarOption() {
                Name = "Reports",
                Path = "/reports",
                Icon = "&#xe555",
                Enabled = true
            }
        };

        public IEnumerable<SidebarOption> Options
        {
            get
            {
                return allOptions;
            }
        }
    }
}
