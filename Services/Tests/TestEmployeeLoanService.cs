﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Interfaces;
using Portal.Models;

namespace Portal.Services
{
    public class TestEmployeeLoanService : IEmployeeLoanService
    {
        public Task<IEnumerable<EmployeeLoan>> GetEmployeeLoansByEmpIDAsync(int empID)
        {
            EmployeeLoan[] empLoans = new EmployeeLoan[]
            {
                new EmployeeLoan
                {
                    RecordID = 1,
                    Selected = false,
                    EmployeeID = 1,
                    LoanID = 1,
                    LoanDate = new DateTime(2021, 02, 01),
                    LoanPrincipalAmount = (decimal) 10000.00,
                    LoanPayableMonth = 12
                },
                new EmployeeLoan
                {
                    RecordID = 2,
                    Selected = false,
                    EmployeeID = 2,
                    LoanID = 1,
                    LoanDate = new DateTime(2021, 02, 02),
                    LoanPrincipalAmount = (decimal) 5000.00,
                    LoanPayableMonth = 12
                }
            };

            return Task.FromResult(empLoans.Where(x => x.EmployeeID == empID));
        }
    }
}
