﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;
using Portal.Interfaces;

namespace Portal.Services
{
    public class TestEmployeePayrollService : IEmployeePayrollService
    {
        public async Task<IEnumerable<EmployeePayroll>> GetEmployeePayrollsAsync()
        {
            EmployeePayroll[] empPayrolls = new EmployeePayroll[]
            {
                new EmployeePayroll
                {
                    RecordID = 1,
                    Selected = false,
                    PayrollID = "0001",
                    EmployeeID = 1,
                    PayrollCutoffDate = new DateTime(2021, 02, 15),
                    Status = 'H',
                    Bonus = (decimal) 5000.00,
                    ThirteenthMonth = (decimal) 5000.00,
                    Allowance = (decimal) 1500.00,
                    Commission = (decimal) 2000.00,
                    LoanID = 1,
                    LoanPayment = (decimal) 1000.00,
                    CashAdvance = (decimal) 2500.00
                },
                new EmployeePayroll
                {
                    RecordID = 2,
                    Selected = false,
                    PayrollID = "0002",
                    EmployeeID = 2,
                    PayrollCutoffDate = new DateTime(2021, 02, 15),
                    Status = 'H',
                    Bonus = (decimal) 2500.00,
                    ThirteenthMonth = (decimal) 5000.00,
                    Allowance = (decimal) 1500.00,
                    Commission = (decimal) 0000.00,
                    LoanID = null,
                    LoanPayment = null,
                    CashAdvance = null
                },
                new EmployeePayroll
                {
                    RecordID = 3,
                    Selected = false,
                    PayrollID = "0003",
                    EmployeeID = 3,
                    PayrollCutoffDate = new DateTime(2021, 02, 15),
                    Status = 'H',
                    Bonus = null,
                    ThirteenthMonth = (decimal) 5000.00,
                    Allowance = (decimal) 1500.00,
                    Commission = (decimal) 2000.00,
                    LoanID = 1,
                    LoanPayment = (decimal) 1000.00,
                    CashAdvance = null
                }
            };
            
            return await Task.FromResult(empPayrolls);
        }
    }
}
