﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;
using Portal.Interfaces;

namespace Portal.Services
{
    public class TestEmployeeTimekeepService : IEmployeeTimekeepService
    {
        public async Task<IEnumerable<EmployeeTimekeep>> GetEmployeeTimekeepsAsync()
        {
            EmployeeTimekeep[] empTimekeeps = new EmployeeTimekeep[]
            {
                new EmployeeTimekeep
                {
                    RecordID = 1,
                    Selected = false,
                    EmployeeID = 1,
                    DateTimeIn = new DateTime(2021, 02, 01, 8, 0, 0),
                    DateTimeOut = new DateTime(2021, 02, 01, 5, 30, 0),
                    RegularHours = 8.30,
                    OvertimeHours = 0.00,
                    LeaveHours = 0.00,
                    TotalHours = 8.30
                },
                new EmployeeTimekeep
                {
                    RecordID = 2,
                    Selected = false,
                    EmployeeID = 2,
                    DateTimeIn = new DateTime(2021, 02, 01, 8, 0, 0),
                    DateTimeOut = new DateTime(2021, 02, 01, 5, 00, 0),
                    RegularHours = 8.00,
                    OvertimeHours = 0.00,
                    LeaveHours = 0.00,
                    TotalHours = 8.00
                },
                new EmployeeTimekeep
                {
                    RecordID = 3,
                    Selected = false,
                    EmployeeID = 3,
                    DateTimeIn = new DateTime(2021, 02, 01, 8, 0, 0),
                    DateTimeOut = new DateTime(2021, 02, 01, 5, 00, 0),
                    RegularHours = 0.00,
                    OvertimeHours = 0.00,
                    LeaveHours = 8.00,
                    TotalHours = 8.00
                }
            };

            return await Task.FromResult(empTimekeeps);
        }
    }
}
