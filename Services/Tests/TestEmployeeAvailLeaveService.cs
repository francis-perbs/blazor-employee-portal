﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;
using Portal.Interfaces;

namespace Portal.Services
{
    public class TestEmployeeAvailLeaveService : IEmployeeAvailLeaveService
    {
        public Task<IEnumerable<EmployeeAvailLeave>> GetEmployeeLeavesByEmpIDAsync(int empID)
        {
            EmployeeAvailLeave[] empLeaves = new EmployeeAvailLeave[]
            {
                new EmployeeAvailLeave
                {
                    RecordID = 1,
                    Selected = false,
                    EmployeeID = 1,
                    LeaveID = 1,
                    Qty = (decimal) 1.0
                },
                new EmployeeAvailLeave
                {
                    RecordID = 2,
                    Selected = false,
                    EmployeeID = 2,
                    LeaveID = 1,
                    Qty = (decimal) 2.0
                }
            };

            return Task.FromResult(empLeaves.Where(x => x.EmployeeID == empID));
        }
    }
}
