﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portal.Models;
using Portal.Interfaces;

namespace Portal.Services
{
    public class TestEmployeeExpenseClaimService : IEmployeeExpenseClaimService
    {
        public async Task<IEnumerable<EmployeeExpenseClaim>> GetEmployeeExpenseClaimsAsync()
        {
            EmployeeExpenseClaim[] empExpenseClaims = new EmployeeExpenseClaim[]
            {
                new EmployeeExpenseClaim
                {
                    RecordID = 1,
                    ExpenseClaimID = "EXPCLM001",
                    RequestDate = new DateTime(2021, 02, 01),
                    ClaimOwnerID = 1,
                    ExpenseClaimDate = new DateTime(2021, 02, 01),
                    Amount = (decimal)500.00,
                    Status = "H",
                    Description = "Expense Claims for January 01, 2021"
                },
                new EmployeeExpenseClaim
                {
                    RecordID = 2,
                    ExpenseClaimID = "EXPCLM002",
                    RequestDate = new DateTime(2021, 02, 04),
                    ClaimOwnerID = 2,
                    ExpenseClaimDate = new DateTime(2021, 02, 03),
                    Amount = (decimal)250.00,
                    Status = "H",
                    Description = "Expense Claims for January 03, 2021"
                },
                new EmployeeExpenseClaim
                {
                    RecordID = 3,
                    ExpenseClaimID = "EXPCLM003",
                    RequestDate = new DateTime(2021, 02, 15),
                    ClaimOwnerID = 1,
                    ExpenseClaimDate = new DateTime(2021, 02, 09),
                    Amount = (decimal)550.00,
                    Status = "H",
                    Description = "Expense Claims for January 09, 2021"
                }
            };

            return await Task.FromResult(empExpenseClaims);
        }
    }
}
