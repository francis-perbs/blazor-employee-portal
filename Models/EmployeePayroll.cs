﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.Models
{
    public class EmployeePayroll
    {
        public int RecordID { get; set; }
        public bool Selected { get; set; }
        public string PayrollID { get; set; }
        public int EmployeeID { get; set; }
        public DateTime PayrollCutoffDate { get; set; }
        public char Status { get; set; }
        public decimal? Bonus { get; set; }
        public decimal? ThirteenthMonth { get; set; }
        public decimal? Allowance { get; set; }
        public decimal? Commission { get; set; }
        public int? LoanID { get; set; }
        public decimal? LoanPayment { get; set; }
        public decimal? CashAdvance { get; set; }
    }
}
