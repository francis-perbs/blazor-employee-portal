﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.Models
{
    public class EmployeeExpenseClaim
    {
        public int RecordID { get; set; }
        public string ExpenseClaimID { get; set; }
        public DateTime RequestDate { get; set; }
        public int ClaimOwnerID { get; set; }
        public DateTime ExpenseClaimDate { get; set; }
        public decimal? Amount { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }

    }
}
