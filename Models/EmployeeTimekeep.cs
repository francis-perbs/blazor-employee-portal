﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.Models
{
    public class EmployeeTimekeep
    {
        public int RecordID { get; set; }
        public bool Selected { get; set; }
        public int EmployeeID { get; set; }
        public DateTime DateTimeIn { get; set; }
        public DateTime DateTimeOut { get; set; }
        public double RegularHours { get; set; }
        public double OvertimeHours { get; set; }
        public double LeaveHours { get; set; }
        public double TotalHours { get; set; }
    }
}
