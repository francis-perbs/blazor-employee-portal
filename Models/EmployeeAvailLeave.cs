﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.Models
{
    public class EmployeeAvailLeave
    {
        public int RecordID { get; set; }
        public bool Selected { get; set; }
        public int EmployeeID { get; set; }
        public int LeaveID { get; set; }
        public decimal Qty { get; set; }
    }
}
