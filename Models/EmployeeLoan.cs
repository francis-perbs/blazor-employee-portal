﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.Models
{
    public class EmployeeLoan
    {
        public int RecordID { get; set; }
        public bool Selected { get; set; }
        public int EmployeeID { get; set; }
        public int LoanID { get; set; }
        public DateTime LoanDate { get; set; }
        public decimal LoanPrincipalAmount { get; set; }
        public double LoanPayableMonth { get; set; }
    }
}
